import { Component } from '@angular/core';

@Component({
  selector: 'high-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front';
}
